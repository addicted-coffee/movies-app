import PropTypes from 'prop-types';


function MovieItem({ movie }) {
  const { title, release_date } = movie;
  return (
    <li>{title} - {release_date}</li>
  );
}

MovieItem.defaultProps = {};

MovieItem.propTypes = {
  movie: PropTypes.shape({
    title: PropTypes.string.isRequired,
    release_date: PropTypes.string.isRequired,
  }),
};

export default MovieItem;
