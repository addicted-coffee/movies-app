import React from 'react';

function Header() {
  return (<header></header>)
}

function Footer() {
  return (<footer></footer>)
}

function Layout({ children }) {
  return (
    <React.Fragment>
     <Header />
     {children}
     <Footer />
    </React.Fragment>
  )
}

export default Layout;
