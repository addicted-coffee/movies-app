import { movie } from '../models';
import { GET_MOVIE, GET_MOVIE_ERROR, GET_MOVIE_SUCCESS } from '../actions/movieActions';


const movieReducer = (state = movie, action = {}) => {
  switch (action.type) {
    case GET_MOVIE:
      return { ...state, isLoading: true };
    case GET_MOVIE_ERROR:
      return { ...state, isLoading: false };
    case GET_MOVIE_SUCCESS:
      return {
        ...state,
        movies: action.payload.data,
        loading: false,
      }
    default:
      return movie
  }
};

export default movieReducer;
