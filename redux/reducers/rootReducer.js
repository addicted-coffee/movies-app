import { combineReducers } from 'redux';

// Reducers
import movieReducer from './movieReducer';

const rootReducer = combineReducers({
  movie: movieReducer,
});

export default rootReducer;
