export const GET_MOVIE = 'movie/GET_MOVIE';
export const GET_MOVIE_ERROR = 'movie/GET_MOVIE_ERROR';
export const GET_MOVIE_SUCCESS = 'movie/GET_MOVIE_SUCCESS';

export const GET_MOVIE_BY_ID = 'movie/GET_MOVIE_BY_ID';
export const GET_MOVIE_BY_ID_ERROR = 'movie/GET_MOVIE_BY_ID_ERROR';
export const GET_MOVIE_BY_ID_SUCCESS = 'movie/GET_MOVIE_BY_ID_SUCCESS';

export const getMovie = payload => ({
  type: GET_MOVIE,
  payload,
});

export const getMovieSuccess = payload => ({
  type: GET_MOVIE_SUCCESS,
  payload,
});

export const getMovieError = payload => ({
  type: GET_MOVIE_ERROR,
  payload,
});

export const getMovieById = (movieId) => ({
  type: GET_MOVIE_BY_ID,
  movieId,
});

export const getMovieByIdError = payload => ({
  type: GET_MOVIE_BY_ID_ERROR,
  payload,
});

export const getMovieByIdSuccess = payload => ({
  type: GET_MOVIE_BY_ID_SUCCESS,
  payload,
});
