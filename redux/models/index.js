import movie from './movie';
import person from './person';

export { movie, person };
