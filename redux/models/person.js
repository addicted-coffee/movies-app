const person = {
  first_name: null,
  last_name: null,
  alias: null,
  error: false,
  id: null,
  loading: false,
};

export default person;
