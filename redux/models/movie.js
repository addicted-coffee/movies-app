const movie = {
  name: null,
  release_year: null,
  error: false,
  id: null,
  loading: false,
};

export default movie;
