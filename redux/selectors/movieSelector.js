import { createSelector } from 'reselect';

const movieSelector = state => state.movie.movies;
export const getMovies = createSelector(
  movieSelector,
  movies => movies,
);
