import { call, takeLatest, put } from 'redux-saga/effects';
import {
  GET_MOVIE,
  getMovieSuccess,
  getMovieError,
  GET_MOVIE_BY_ID,
  getMovieByIdError,
  getMovieByIdSuccess,
} from '../actions/movieActions';
import { getMovieAPI, getMovieByIdAPI } from './api';

export function* getMovie(action) {
  try {
    const request = yield call(getMovieAPI);
    yield put(getMovieSuccess(request));
  } catch (error) {
    yield put(getMovieError(error));
  }
}

export function* getMovieWithId(action) {
  try {
    const request = yield call(getMovieByIdAPI, action.movieId);
    yield put(getMovieByIdSuccess(request));
  } catch (error) {
    yield put(getMovieByIdError(error));
  }
}

export function* movieSaga() {
  yield takeLatest(GET_MOVIE, getMovie);
  yield takeLatest(GET_MOVIE_BY_ID, getMovieWithId);
}

export default movieSaga;
