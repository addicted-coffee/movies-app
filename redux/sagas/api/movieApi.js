import axios from './axios';

export const getMovieAPI = async () => {
  return await axios.get("/movies/");
};

export const getMovieByIdAPI = async movieId => {
  return await axios.get(`/movies/${movieId}`);
};
