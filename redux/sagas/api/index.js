import {
  getMovieAPI,
  getMovieByIdAPI,
} from './movieApi';

export {
  // movie APIs
  getMovieAPI,
  getMovieByIdAPI,
};
