import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import MovieItem from '../../components/MovieItem';

import {
  getMovie,
} from '../../redux/actions/movieActions';

import {
  getMovies,
} from '../../redux/selectors/movieSelector'

const Movies = () => {
  const [isLoading] = useState(false);
  const dispatch = useDispatch();
  const movies = useSelector(getMovies);

  useEffect(() => {
    dispatch(getMovie());
  }, []);

  return (
    <React.Fragment>
      {isLoading ? (
        <div>loading</div>
      ) : (
        movies && movies.map(movie => <MovieItem movie={movie} key={movie.id} />)
      )}
    </React.Fragment>
  )
}

export default Movies;
