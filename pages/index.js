import Head from 'next/head'


export default function Home() {
  return (
    <div>
      <Head>
        <title>Movies!</title>
      </Head>

      <main>
        <h1>
          Movies!
        </h1>

        <ul>
          <li>
            <a href="#">Movie Name</a>
          </li>
        </ul>
      </main>

      <footer>
        <a
          href="https://addicted.coffee"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' AddictedCoffee '}
        </a>
      </footer>
    </div>
  )
}
